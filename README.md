# Nihongo

## Description
Menu:
![](http://p1.bqimg.com/567571/0ae54f2f45e4b220.jpg)

This is a Japanese language learning game. There are three mini games on this projects. Built with Cordova.

* Jigsaw: Learn new Japanese words by draging japanese characters in the right order.
![](http://ww3.sinaimg.cn/large/0060lm7Tgy1fbwcmw8kjtj30dw07tdha.jpg)

* Rolling Game: Roll the ball to the right basket to test if you have memorized the word.
![](http://p1.bqimg.com/567571/6920833933b7d71d.jpg)
![](http://p1.bqimg.com/567571/5326fb580520fb31.jpg)

* Card Game: Choose the correct picture to test if you have memorized the word.
![](http://p1.bqimg.com/567571/f1d5030d6bb05195.jpg)

## Author
Chen Zhi

## Mentors
* Derek O'Reilly, Dundalk Institute of Technology (DkIT), Dundalk, Co. Louth, Ireland.
* Krzysztof Podlaski, University of Lodz, Poland.

## Logo & Splash Screen
Logo:
![](http://p1.bpimg.com/567571/4cc6befb8332f2a6.png)

Splash Screen: 
![](http://p1.bqimg.com/567571/43efd82e1d61af45.png)

## Technology
* Phonegap
* Bootstrap
* jQuery
* 极简图床
* Markdown (MacDown)